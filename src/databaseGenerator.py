# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.utils import shuffle

f = pd.read_csv("HTRU_2.csv", header=None)
pulsar = f.loc[f[8] == 1]
noPulsar =  f.loc[f[8] == 0]

pulsar = shuffle(pulsar)
noPulsar = shuffle(noPulsar)

#testDB -> 322 pulsares - 3258 no pulsares
#validationDB 258 pulsares - 2606 no pulsares
#trainingDB -> 1031 pulsares - 10423 no pulsares

testDB = pd.concat([pulsar.iloc[0:322, :], noPulsar.iloc[0:3258, :]])
validationDB = pd.concat([pulsar.iloc[322:580, :], noPulsar.iloc[3258:5864, :]])
trainingDB = pd.concat([pulsar.iloc[580:, :], noPulsar.iloc[5864:, :]])

testDB.to_csv("testDB.csv", header=None, index=False)
validationDB.to_csv("validationDB.csv", header=None, index=False)
trainingDB.to_csv("trainingDB.csv", header=None, index=False)

print 'done'