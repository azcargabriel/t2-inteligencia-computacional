from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def decider(value, threshold):
    if value >= threshold:
        return 1
    return 0

#Aqui guardaremos las tasas TVP y TFP para cada degree
tvps_d2 = []
tfps_d2 = []
tvps_d3 = []
tfps_d3 = []
tvps_d4 = []
tfps_d4 = []
tvps_d5 = []
tfps_d5 = []
tvps_d6 = []
tfps_d6 = []

for k in [2,3,4,5,6]:
    
    #Entrenamiento
    f = pd.read_csv("trainingDB.csv", header=None)
    X = StandardScaler().fit_transform(f.values[:, 0:8])
    y = f.values[:, 8]
    clf = svm.SVC(kernel='poly', degree=k)
    clf.fit(X, y)
    
    #Pruebas para curvas ROC
    f2 = pd.read_csv("validationDB.csv", header=None)
    xValidation = StandardScaler().fit_transform(f2.values[:, 0:8])
    yValidation = f2.values[:, 8]
    positivesQuantity = (yValidation == 1).sum()
    negativesQuantity = (yValidation == 0).sum()
    scores = clf.decision_function(xValidation)
    
    tvps = []
    tfps = []
    
    #Calculamos TFP y TVP, con umbral i
    for i in np.linspace(-40.0, 40.0, num=4000):
        #Aplicamos decider a cada uno de los scores
        mapArray = np.vectorize(decider)
        classes = mapArray(scores, i)
        
        fp = 0
        vp = 0
        j = 0
        while(j < classes.size):
            if(classes[j] == 1 and yValidation[j] == 1):
                vp = vp + 1
            elif(classes[j] == 1 and yValidation[j] == 0):
                fp = fp + 1
            j = j + 1
    
        tvps.append(float(vp)/positivesQuantity)
        tfps.append(float(fp)/negativesQuantity)
    
    if(k == 2):
        tfps_d2 = tfps
        tvps_d2 = tvps
    elif(k == 3):
        tfps_d3 = tfps
        tvps_d3 = tvps
    elif(k == 4):
        tfps_d4 = tfps
        tvps_d4 = tvps
    elif(k == 5):
        tfps_d5 = tfps
        tvps_d5 = tvps
    elif(k == 6):
        tfps_d6 = tfps
        tvps_d6 = tvps
        
        
plt.plot(tfps_d2, tvps_d2, label='Grado 2')
plt.plot(tfps_d3, tvps_d3, label='Grado 3')
plt.plot(tfps_d4, tvps_d4, label='Grado 4')
plt.plot(tfps_d5, tvps_d5, label='Grado 5')
plt.plot(tfps_d6, tvps_d6, label='Grado 6')
plt.legend()
plt.title("Curvas ROC SVM polinomial")
plt.xlabel("TFP")
plt.ylabel("TVP")
plt.savefig('Polynomial_ROC.png', format='png', dpi=1000)
plt.show()
