from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def decider(value, threshold):
    if value >= threshold:
        return 1
    return 0

#Aqui guardaremos las tasas TVP y TFP
tvps_0 = []
tfps_0 = []
tvps_1 = []
tfps_1 = []
tvps_2 = []
tfps_2 = []
tvps_3 = []
tfps_3 = []

for g in [0.001,0.01,0.1,0.2]:
    #Entrenamiento
    f = pd.read_csv("trainingDB.csv", header=None)
    X = StandardScaler().fit_transform(f.values[:, 0:8])
    y = f.values[:, 8]
    clf = svm.SVC(kernel='rbf', gamma=g)
    clf.fit(X, y)
    
    #Pruebas para curvas ROC
    f2 = pd.read_csv("validationDB.csv", header=None)
    xValidation = StandardScaler().fit_transform(f2.values[:, 0:8])
    yValidation = f2.values[:, 8]
    positivesQuantity = (yValidation == 1).sum()
    negativesQuantity = (yValidation == 0).sum()
    scores = clf.decision_function(xValidation)

    tvps = []
    tfps = []
    #Calculamos TFP y TVP, con umbral i
    for i in np.linspace(-50.0, 50.0, num=1000):
        #Aplicamos decider a cada uno de los scores
        mapArray = np.vectorize(decider)
        classes = mapArray(scores, i)
        
        fp = 0
        vp = 0
        j = 0
        while(j < classes.size):
            if(classes[j] == 1 and yValidation[j] == 1):
                vp = vp + 1
            elif(classes[j] == 1 and yValidation[j] == 0):
                fp = fp + 1
            j = j + 1
        tvps.append(float(vp)/positivesQuantity)
        tfps.append(float(fp)/negativesQuantity)

    if(g == 0.001):
        tfps_0 = tfps
        tvps_0 = tvps        
    elif(g == 0.01):
        tfps_1 = tfps
        tvps_1 = tvps        
    elif(g == 0.1):
        tfps_2 = tfps
        tvps_2 = tvps
    elif(g == 0.2):
        tfps_3 = tfps
        tvps_3 = tvps

plt.plot(tfps_0, tvps_0, label='sigma 0.001')
plt.plot(tfps_1, tvps_1, label='sigma 0.01')
plt.plot(tfps_2, tvps_2, label='sigma 0.1')
plt.plot(tfps_3, tvps_3, label='sigma 0.2')
plt.legend()
plt.title("Curvas ROC SVM RBF")
plt.xlabel("TFP")
plt.ylabel("TVP")
plt.savefig('RBF_ROC.png', format='png', dpi=1000)
plt.show()

