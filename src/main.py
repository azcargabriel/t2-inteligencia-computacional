import math
from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def decider(value, threshold):
    if value >= threshold:
        return 1
    return 0

def distance(x1, y1, x2, y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)

#Datos
f = pd.read_csv("trainingDB.csv", header=None)
X = StandardScaler().fit_transform(f.values[:, 0:8])
y = f.values[:, 8]

#Entrenamiento lineal
clfL = svm.SVC(kernel='linear')
clfL.fit(X, y)

#Entrenamiento RBF
clfRBF = svm.SVC(kernel='rbf', gamma=0.01)
clfRBF.fit(X, y)

#Entrenamiento polinomial
clfP = svm.SVC(kernel='poly', degree=3)
clfP.fit(X, y)

#Prueba para curva ROC
f2 = pd.read_csv("validationDB.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:8])
yValidation = f2.values[:, 8]

#Cantidad de datos positivos y negativos
positivesQuantity = (yValidation == 1).sum()
negativesQuantity = (yValidation == 0).sum()

#Aqui guardaremos las tasas TVP y TFP
tvpsL = []
tfpsL = []
tvpsRBF = []
tfpsRBF = []
tvpsP = []
tfpsP = []

#Calculamos TFP y TVP, con umbral i para SVM Lineal
scores = clfL.decision_function(xValidation)
for i in np.linspace(-20.0, 20.0, num=1000):
    #Aplicamos decider a cada uno de los scores
    mapArray = np.vectorize(decider)
    classes = mapArray(scores, i)
    
    fp = 0
    vp = 0
    j = 0
    while(j < classes.size):
        if(classes[j] == 1 and yValidation[j] == 1):
            vp = vp + 1
        elif(classes[j] == 1 and yValidation[j] == 0):
            fp = fp + 1
        j = j + 1
    tvpsL.append(float(vp)/positivesQuantity)
    tfpsL.append(float(fp)/negativesQuantity)
    

#Calculamos TFP y TVP, con umbral i para SVM RBF
scores = clfRBF.decision_function(xValidation)
for i in np.linspace(-50.0, 50.0, num=1000):
    #Aplicamos decider a cada uno de los scores
    mapArray = np.vectorize(decider)
    classes = mapArray(scores, i)
    
    fp = 0
    vp = 0
    j = 0
    while(j < classes.size):
        if(classes[j] == 1 and yValidation[j] == 1):
            vp = vp + 1
        elif(classes[j] == 1 and yValidation[j] == 0):
            fp = fp + 1
        j = j + 1
    tvpsRBF.append(float(vp)/positivesQuantity)
    tfpsRBF.append(float(fp)/negativesQuantity)
    
#Calculamos TFP y TVP, con umbral i para SVM Polinomial
scores = clfP.decision_function(xValidation)
for i in np.linspace(-40.0, 40.0, num=4000):
    #Aplicamos decider a cada uno de los scores
    mapArray = np.vectorize(decider)
    classes = mapArray(scores, i)
    
    fp = 0
    vp = 0
    j = 0
    while(j < classes.size):
        if(classes[j] == 1 and yValidation[j] == 1):
            vp = vp + 1
        elif(classes[j] == 1 and yValidation[j] == 0):
            fp = fp + 1
        j = j + 1
    tvpsP.append(float(vp)/positivesQuantity)
    tfpsP.append(float(fp)/negativesQuantity)
    
    
minIndexL = 0
minDistance = 99999999
i = 0
while(i < len(tvpsL)):
    d = distance(0, 1, tfpsL[i], tvpsL[i])
    if(d < minDistance):
        minIndexL = i
        minDistance = d
    i = i + 1
    
minIndexRBF = 0
minDistance = 99999999
i = 0
while(i < len(tvpsRBF)):
    d = distance(0, 1, tfpsRBF[i], tvpsRBF[i])
    if(d < minDistance):
        minIndexRBF = i
        minDistance = d
    i = i + 1
    


plt.plot(tfpsL, tvpsL, label="Lineal")
plt.plot(tfpsRBF, tvpsRBF, label="RBF")
plt.plot(tfpsP, tvpsP, label="Polinomial")
plt.legend()
plt.title("Curva ROC SVM")
plt.xlabel("TFP")
plt.ylabel("TVP")
plt.savefig('comparison.png', format='png', dpi=1000)
plt.show()