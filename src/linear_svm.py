from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def decider(value, threshold):
    if value >= threshold:
        return 1
    return 0


#Entrenamiento
f = pd.read_csv("trainingDB.csv", header=None)
X = StandardScaler().fit_transform(f.values[:, 0:8])
y = f.values[:, 8]
clf = svm.SVC(kernel='linear')
clf.fit(X, y)

#Prueba para curva ROC
f2 = pd.read_csv("validationDB.csv", header=None)
xValidation = StandardScaler().fit_transform(f2.values[:, 0:8])
yValidation = f2.values[:, 8]
scores = clf.decision_function(xValidation)

#Cantidad de datos positivos y negativos
positivesQuantity = (yValidation == 1).sum()
negativesQuantity = (yValidation == 0).sum()

#Aqui guardaremos las tasas TVP y TFP
tvps = []
tfps = []

#Calculamos TFP y TVP, con umbral i
for i in np.linspace(-20.0, 20.0, num=1000):
    #Aplicamos decider a cada uno de los scores
    mapArray = np.vectorize(decider)
    classes = mapArray(scores, i)
    
    fp = 0
    vp = 0
    j = 0
    while(j < classes.size):
        if(classes[j] == 1 and yValidation[j] == 1):
            vp = vp + 1
        elif(classes[j] == 1 and yValidation[j] == 0):
            fp = fp + 1
        j = j + 1
    tvps.append(float(vp)/positivesQuantity)
    tfps.append(float(fp)/negativesQuantity)

plt.plot(tfps, tvps)
plt.legend()
plt.title("Curva ROC SVM Lineal")
plt.xlabel("TFP")
plt.ylabel("TVP")
plt.savefig('Linear_ROC.png', format='png', dpi=1000)
plt.show()


