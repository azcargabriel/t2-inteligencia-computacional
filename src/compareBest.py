import math
from sklearn import svm
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def decider(value, threshold):
    if value >= threshold:
        return 1
    return 0

def distance(x1, y1, x2, y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)

#Datos
f = pd.read_csv("trainingDB.csv", header=None)
X = StandardScaler().fit_transform(f.values[:, 0:8])
y = f.values[:, 8]

#Entrenamiento lineal
clfL = svm.SVC(kernel='linear')
clfL.fit(X, y)

#Entrenamiento RBF
clfRBF = svm.SVC(kernel='rbf', gamma=0.01)
clfRBF.fit(X, y)

#Entrenamiento polinomial
clfP = svm.SVC(kernel='poly', degree=3)
clfP.fit(X, y)

#Prueba para curva ROC
f2 = pd.read_csv("testDB.csv", header=None)
xTest = StandardScaler().fit_transform(f2.values[:, 0:8])
yTest = f2.values[:, 8]

#Cantidad de datos positivos y negativos
positivesQuantity = (yTest == 1).sum()
negativesQuantity = (yTest == 0).sum()

#Aqui guardaremos las tasas TVP y TFP
tvpsL = 0
tfpsL = 0
tvpsRBF = 0
tfpsRBF = 0

#Calculamos TFP y TVP, con umbral maximo para SVM Lineal
scores = clfL.decision_function(xTest)

#Aplicamos decider a cada uno de los scores
mapArray = np.vectorize(decider)
classes = mapArray(scores, -1.221)

fp = 0
vp = 0
j = 0
while(j < classes.size):
    if(classes[j] == 1 and yTest[j] == 1):
        vp = vp + 1
    elif(classes[j] == 1 and yTest[j] == 0):
        fp = fp + 1
    j = j + 1
tvpsL = float(vp)/positivesQuantity
tfpsL = float(fp)/negativesQuantity
    

#Calculamos TFP y TVP, con umbral maximo para SVM RBF
scores = clfRBF.decision_function(xTest)
#Aplicamos decider a cada uno de los scores
mapArray = np.vectorize(decider)
classes = mapArray(scores, -1.051)

fp = 0
vp = 0
j = 0
while(j < classes.size):
    if(classes[j] == 1 and yTest[j] == 1):
        vp = vp + 1
    elif(classes[j] == 1 and yTest[j] == 0):
        fp = fp + 1
    j = j + 1
tvpsRBF = float(vp)/positivesQuantity
tfpsRBF = float(fp)/negativesQuantity

plt.plot(tfpsL, tvpsL, 'o-', label="Lineal")
plt.plot(tfpsRBF, tvpsRBF, 'o-', label="RBF")
plt.legend()
plt.title("Lineal vs RBF")
plt.xlabel("TFP")
plt.ylabel("TVP")
plt.ylim(0,1)
plt.xlim(0,1)
plt.savefig('best.png', format='png', dpi=1000)
plt.show()
