T2 Inteligencia Computacional 2018-2
Gabriel Andrés Azócar Cárcamo

Para partir la base de datos, se debe tener un archivo llamado HTRU_2.csv y correr:
python databaseGenerator.py

- Lo primero que se debe tener son tres archivos csv, trainingDB.csv, validationDB.csv y testDB.csv (los genera databaseGenertor), con esos nombres.

- Luego, basta correr el script que se quiera, por ejemplo si se quiere ver la curva ROC de SVM polinomial:
    python polinomial_svm.py
Esto mostrará un gráfico y además guardará ese mismo en un archivo .png.

- Para ver la comparación entre los tres, basta con hacer:
    python main.py
Que guardará un archivo .png con la vista.

- Para comparar RBF con Lineal (usando los umbrales descritos en el informe) usando testDB.csv, basta hacer:
    python compareBest.py
Que guardará un archivo .png con la comparación.